import QtQuick 2.5
import QtQuick.Controls 2.5
import UIControls 1.0
import QtQuick.Layouts 1.1

Item {
    id: docroot
    height: totalHeight
    property int totalHeight: column.height
    property alias header: headerSpace.children
    property alias contentHeight: contentWrapper.height
    property alias contentWidth: contentWrapper.width
    default property alias content: contentWrapper.children

    property alias managedContent: container.children
    property int radius
    property string helpText
    property string headline
    property int margins: 20

    property int topMargin: margins

    property bool doLayout: false

    property alias spacing: container.spacing

    function showError() {
        errorAnimation.start()
    }

    Rectangle {
        width: parent.width
        height: parent.height
        anchors.bottom: parent.bottom
        color: Colors.darkBlue
    }
    Item {
        id: content
        anchors.fill: parent

        SequentialAnimation {
            id: errorAnimation
            ColorAnimation {
                target: docroot
                property: "border.color"
                from: "transparent"
                to: Colors.warnRed
                duration: 50
            }
            PauseAnimation {
                duration: 200
            }
            ColorAnimation {
                target: docroot
                property: "border.color"
                to: "transparent"
                from: Colors.warnRed
                duration: 50
            }
            ColorAnimation {
                target: docroot
                property: "border.color"
                from: "transparent"
                to: Colors.warnRed
                duration: 50
            }
            PauseAnimation {
                duration: 200
            }
            ColorAnimation {
                target: docroot
                property: "border.color"
                to: "transparent"
                from: Colors.warnRed
                duration: 50
            }
        }

        Column {
            id: column
            width: parent.width
            Rectangle {
                visible: headline !== ""
                color: Colors.greyBlue
                radius: docroot.radius
                height: 40
                width: parent.width

                RowLayout {
                    id: header
                    anchors.margins: docroot.margins
                    anchors.right: parent.right
                    anchors.left: parent.left
                    height: parent.height
                    spacing: 0

                    TextLabel {
                        id: headlineLabel
                        font.family: Fonts.simplonNorm_Medium
                        text: docroot.headline
                        color: Colors.white
                        fontSize: Fonts.subHeaderFontSize
                    }

                    HelpTextFlyout {
                        height: 40
                        width: docroot.helpText == "" ? 0 : 30
                        flyoutWidth: header.width > 340 ? 300 : header.width - 40
                        triangleDelta: headlineLabel.width + 8
                        helpText: docroot.helpText
                        flyoutParent: header
                        flyoutX: 0
                    }

                    Item {
                        id: headerSpace
                        Layout.fillHeight: true
                        Layout.fillWidth: true
                    }
                }
            }

            ContainerBase {
                id: container
                width: parent.width
                margins: docroot.margins
                topMargin: docroot.topMargin
                height: {
                    if (docroot.doLayout)
                        return docroot.height - 40
                    else
                        return contentWrapper.height + 2 * docroot.margins
                }

                Column {
                    id: contentWrapper
                    width: parent.width
                }
            }
        }
    }
}

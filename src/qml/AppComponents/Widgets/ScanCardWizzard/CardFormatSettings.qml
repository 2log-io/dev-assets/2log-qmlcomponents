import QtQuick 2.12
import CloudAccess 1.0
import QtQuick.Controls 2.3
import UIControls 1.0

IconButton {
    id: settings

    function prepareCardID(cardID)
    {
        var isDec = cardFormat.selectedIndex === 1
        return cppHelper.prepareCardID(cardID,swapBytes.checked,isDec)
    }

    icon: Icons.settings
    iconColor: Colors.white_op30
    onClicked: flyout.open()
    FlyoutBox
    {
        id: flyout
        Form
        {
            width: 200
            FormDropDownItem
            {
                id: cardFormat
                label: qsTr("Format")
                selectedIndex: 0
                options:[qsTr("Hexadezimal"), qsTr("Dezimal")]
                onEditedSelectedIndexChanged:settingsModel.setProperty("selectedIndex", editedSelectedIndex)
            }

            FormCheckItem
            {
                id: swapBytes
                label: qsTr("Swap Bytes")
                checked: true
                onEditedCheckedChanged:settingsModel.setProperty("swapBytes", editedChecked)
            }
        }
    }

    SynchronizedObjectModel {
        id: settingsModel
        resource: "public/settings/readcardoptions"
        onInitializedChanged: {
            if(!initialized)
                return;

            cardFormat.selectedIndex = Qt.binding(function () {
                var val = settingsModel.selectedIndex
                return val === undefined ? 0 : val
            })

            swapBytes.checked = Qt.binding(function () {
                var val = settingsModel.swapBytes
                return val === undefined ? false : val
            })
        }
    }
}

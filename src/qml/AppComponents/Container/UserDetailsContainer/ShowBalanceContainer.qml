

/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 2.3
import UIControls 1.0
import CloudAccess 1.0
import "../../Widgets"

Container {
    id: docroot
    headline: qsTr("Kontostand")

    property int referenceHeight
    property SynchronizedObjectModel userModel

    Item {
        height: parent.height
        width: parent.width

        Column {
            anchors.centerIn: parent
            spacing: 30
            Row {
                spacing: 10
                anchors.horizontalCenter: parent.horizontalCenter

                TextLabel {
                    Binding on text {
                        value: (userModel.balance / 100).toLocaleString(
                                   Qt.locale("de_DE"))
                    }
                    fontSize: Fonts.bigDisplayFontSize
                }

                TextLabel {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.verticalCenterOffset: 5
                    text: "EUR"
                    color: Colors.grey
                    opacity: .4
                    fontSize: Fonts.bigDisplayUnitFontSize
                }
            }
        }
    }
}
